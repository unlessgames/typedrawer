class v2
  constructor:(@x, @y) ->
  dist : (a) -> Math.sqrt(Math.pow(@x - a.x, 2) + Math.pow(@y - a.y, 2))
dir = (a, b) -> new v2(a.x - b.x, a.y - b.y)
dist = (a, b) -> Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2))
ctx = $("#canvas").get(0).getContext "2d"
btx = $("#brushcanvas").get(0).getContext "2d"
# toolbar = $("#toolbar").html "ffs"
text = ""
mousedown = false
firstdown = false
previewbrush = true

splitmenuitems = [
  {name:"letters", html:"*"},
  {name:"words", html:"*****"},
  {name:"sentences", html:"* **** *** ** ******"}
]
casemenuitems = [
  {name:"keep", html:"Aa"},
  {name:"lower", html:"aa"},
  {name:"upper", html:"AA"}
]
toolmenuitems = [
  {name:"freehand", html:"freehand"},
  {name:"spray", html:"spray"}
]
brush =
  position : new v2(0,0)
  split:"letters"
  casing:"keep"
  rotation: 0
  fontsize : 14
  color : "#000"
  text : ""
  tool : "freehand"



picked = ""
pickstart = value : 0, p : new v2(0,0)
picking = false

splitter = (txt, type) ->
  switch type
    when "letters"
      txt.split ""
    when "words"
      r = txt.split(/[\.\s\?\!,]{1,9999}/)
      r.pop()
      r
      # txt.split(/[\.\s\?\!,]{1,9999}(?!$)/)
    when "sentences"
      txt.split /(?<!\w\.\w.)(?<!\w\.)(?<=\.|\?|!)\s/
    else
      txt.split ""

settings = 
  step : "letter"

actions = []

drawtext = (tx, br, save = true) ->
  if save then actions.push(JSON.parse(JSON.stringify br))
  tx.save()
  tx.translate(br.position.x, br.position.y)
  tx.rotate(br.rotation)
  tx.textAlign = "left"
  tx.font="#{br.fontsize}px monospace";
  tx.fillStyle = br.color
  tx.fillText(br.text, 0, br.fontsize * 0.5)
  tx.restore()

selector = (s,e) ->
  field = $("#sourcetext").get(0)
  field.focus()
  # field.setSelectionRange(s,e)
  field.selectionStart = s
  field.selectionEnd = e

wrap = (arr, i, d) ->
  if i + d < 0 then arr[arr.length + (i + d)]
  else arr[(i + d) % arr.length]

tools = 
  spray :
    current : 0
    nexttime : 0
    rate : 100
    draw : (m, time) ->
      if mousedown
        if @nexttime < time or firstdown
          firstdown = false
          @nexttime = Date.now() + @rate
          if text.length > 0
            brush.text = text[@current]
            drawtext(ctx, brush, m)
            @current = (@current+1) % text.length
            console.log text[@current]
  freehand :
    current : 0
    lastpos : new v2(0,0)
    draw : (m) ->
      if mousedown
        if firstdown or dist(@lastpos, m) > brush.fontsize * 0.62 * wrap(text, @current, -1).length
          console.log Math.atan2(dir(m, @lastpos).y, dir(m, @lastpos).x)
          if firstdown 
            firstdown = false
          else
            brush.rotation = Math.atan2(dir(m, @lastpos).y, dir(m, @lastpos).x)
          @lastpos = new v2(m.x, m.y)
          brush.text = text[@current]
          drawtext(ctx, brush, m)
          @current = if text.length is 1 then 0 else (@current+1) % text.length

line = (tx, a, b, c, w = 1) ->
  tx.strokeStyle = c
  tx.lineWidth = w
  tx.beginPath()
  tx.moveTo(a.x, a.y)
  tx.lineTo(b.x, b.y)
  tx.stroke()

draw = () ->
  btx.fillStyle = "rgba(0,0,0,0)"
  btx.clearRect(0,0, btx.canvas.width, btx.canvas.height)  
  brush.text = if text[tools[brush.tool].current] is " " then "_" else text[tools[brush.tool].current]
  if previewbrush then drawtext(btx, brush, false)
  if picking then line(btx, brush.position, pickstart.p, "#000")
  tools[brush.tool].draw(brush.position, Date.now())
  window.requestAnimationFrame draw

resizecanvas = (tx) ->
  tx.canvas.width = window.innerWidth
  tx.canvas.height = window.innerHeight

casify = (t, c) ->
  switch c
    when "upper" then t.toUpperCase()
    when "lower" then t.toLowerCase()
    else t

grabtext = (br) ->
  tools[br.tool].current = 0 # TODO
  text = splitter(casify($("#sourcetext").val(), br.casing), br.split)

keys = (e, down) ->
  switch e.key
    # when "n" 
    #   if confirm("create new? your drawing will be lost!")
    #     ctx.clearRect(0,0,ctx.canvas.width, ctx.canvas.height)    
    when "t"
      if firstdown then previewbrush = !previewbrush
    when "s"
      if down and not picking then pickstart = value : brush.fontsize, p : new v2(brush.position.x, brush.position.y)
      picking = down
      picked = "fontsize"
    when "r"
      if down and not picking then pickstart = value : brush.rotation, p : new v2(brush.position.x, brush.position.y)
      picking = down
      picked = "rotation"
  

menus = {}

select = (m, v) ->
  menus[m].select(v)

changebrush = (n, v) ->
  console.log "#{n} , #{v}"
  brush[n] = v
  grabtext(brush)


splitbuttontext = (t) ->
  switch t
    when "letters" then "*"
    when "words" then "*****"
    when "sentences" then "* **** *** ** ******"
    else t    
  
class menu
  buttons : []
  name : ""
  current : ""
  select : (s) ->
    # s = e.currentTarget.id.split("_")[1]
    @current = s
    for v in @buttons
      if v.name is s then $("#"+@name+"_"+v.name).addClass "selected"
      else $("##{@name}_#{v.name}").removeClass "selected"
    changebrush @name, @current
  insta : (n,bs) ->
    btdoms = bs.map((v) -> $("<div>", { id: "#{n}_#{v.name}", html:v.html, class:"menubutton"}))
    $("#toolbar").append($("<div>", {id:n, class:"menu",html:btdoms, })) #style:(if n is "casing" then "width:4em" else "")
    btdoms.map((v) -> $("##{v.get(0).id}").click((e) -> select(n, e.currentTarget.id.split("_")[1])))
  constructor : (n, vs, @def) ->
    @buttons = vs
    @name = n
    @insta(n, vs)

pick = (p) ->
  switch p
    when "fontsize"
      dist(brush.position, pickstart.p)
    when "rotation"
      d = dir(brush.position, pickstart.p)
      Math.atan2(d.y, d.x)
    else 
      undefined

bindevents = () ->
  $("html").mousedown(()-> 
                        mousedown = true
                        firstdown = true
  )
  $("html").mouseup(()-> mousedown = false)
  $("html").mousemove((e) -> 
                      brush.position = new v2(e.clientX, e.clientY)
                      if picking 
                        brush[picked] = pick picked
                        $("##{picked}").val brush[picked]
                      )
  $("html").keydown((e) -> keys(e, true))
  $("html").keyup((e) -> keys(e, false))
  $("#sourcetext").bind("input propertychange", () -> if $("#sourcetext").val().length isnt 0 then grabtext(brush))
  $("#fontsize").bind("input propertychange", (e) -> brush.fontsize =e.currentTarget.value)

init = () ->
  grabtext(brush)
  resizecanvas ctx 
  resizecanvas btx
  bindevents()
  ctx.fillStyle = "#ddd"
  ctx.fillRect(0,0,ctx.canvas.width, ctx.canvas.height)
  menus.split = new menu("split", splitmenuitems, 0)
  menus.casing = new menu("casing", casemenuitems, 0)
  menus.tool = new menu("tool", toolmenuitems, 0)
  for k in Object.keys(menus)
    select(k, brush[k])
  draw()

init()

